function view_zygo(varargin)
%% Zygo data viewer
% To open a datafile acquired by the Zygo optical profiler in the cleanroom at
% UU (i don't really remember the model atm) just call this function without
% arguments, select the file you want to view and open. You can export line data
% by right clicking the lower right hand table and surface data by right
% clicking either the flat or 3D-surface view. Note that the controls to add and
% delete lines and change lateral units are located in the toolbar just below
% the menu bar.
%
% Other ways of calling view_zygo:
%	% using the file name
%	view_zygo('/path/to/data/file.datx')
%	% or by loading data manually
%	z = load_zygo('/path/to/data/file.datx');
%	view_zygo(z)
%
% Note on set manipulations
% There are some not terribly advanced methods for manipulating the data sets
% available in the GUI. These only operate on the _plotted_ sets and are not
% stored anywhere - if you want to save the manipulated data you have to export
% it (rightclick on it and select the type of export you want, file or to
% workspace). If you on the other hand want to revert your manipulations you
% simply select another set and then back again. If you wanna read more about
% the interpolation see `doc scatteredInterpolant`
%
% Note about javacomponent
% Since R2019b matlab has started issuing warnings about javacomponent being
% deprecated, i've chosen to silence this warning in here. I dunno how
% mathwork's gonna deal with the whole oracle debacle but should they choose
% to deprecate javacomponent (and i would assume javaObjectEDT, etc.) shit's
% gonna break.
%
% written by ragnar.seton@angstrom.uu.se
%
% TODO
% - [SERIOUS BUG!!!!!!] when switching data set the position of the lines as
%	returned by getPosition is garbeled in someway! exporting data after
%	switching back and forth (at least after having interpolated values)
%	basically returns garbage.
% 
% - [rework] replace imline with image.roi.Line
% - [functionality] add options rotate, flip, and mirror data in main view.
% - [backend] add a static last_id to log_fun (doesn't matter that it's not
%	thread safe) such that you can call it with row id < 0 and it will append it
%	to the last entry
% - [backend] add logical return to export_dat such that the caller can provide
%   feedback in the log to the user (or an error message).
% - [backend/functionality] add z-converters to the convs and a
%	parser for the strings, 'NanoMeters' is known and from that we could
%	probably guess 'MicroMeters' and such..
% - [functionality] support opening multiple files (and/or maybe folders), they
%	can just be put in a new drop down to the left of add lines-button
% - [functionality] operations on groups of lines:
%	- unify/set length
%	- unify/set angle
%	- (in graph) align around falling or rising point, for missing data points
%	  you can choose, last before drop, first after, or frac f in between
% - [functionality] surface compensation, basically a wizard where you:
%	- let user choose to either:
%		- select one of the existing lines (if available)
%		- hide all other lines and draw new one, and when done click "ok"
%	- let user fit a polynomial to that line, or parts of it
%	- adjust surface along that line (so poly in angle of line)
% - [functionality] add help button
% - [functionality/UI] if software OpenGL-rendering is active notify user about
%					   it being super slow
% - [UI] X and YTicks on the main surface
% - [UI] move "Set manipulation" into "Selected set" such that it has three
%	tabs, "Attributes", "Interpolate", "Normalize"
% - [UI] merge "Paths" and "Data sets" into "Data sets (paths)" (just use "<No
%   name> (/path/of/set)" for the ones without names) and make it half the size
% - [UI] move "Line props" into left column and redesign it... in some way
% - [UI] move 3D and line plots into a panel (make them tabs) in the left column 
% - [UI] switch to icons for add and delete line buttons
% - [UI] check out javax swing doc for the drop down to make it not stretch
% - [UI] check line table highlight color for windows
	
	if (nargin > 0 && isa(varargin{1}, 'struct'))
		z = varargin{1};
		assert(all(isfield(z, {'read', 'path', 'set', 'attributes', 'links'})) && ...
			   all(isfield(z.links, {'name', 'idx'})), ...
			   'Input is not a struct from load_zygo');
		fn = '<input data>';
		full_fn = '';
	elseif (nargin > 0 && (isa(varargin{1}, 'char') || ...
						   isa(varargin{1}, 'string')))
		full_fn = char(varargin{1});
		fn_idx = find(full_fn == filesep, 1, 'last')+1;
		if (~isempty(fn_idx)); fn = full_fn(fn_idx : end);
		else; fn = full_fn;
		end
	else
		assert(nargin == 0, 'Unkown input argument');
		[f, p] = file_dlg(@uigetfile, ...
						  { '*.datx', ...
							'Zygo data (*.datx)'; ...
							'*.h5;*.hdf5;*.he5;*.hdf', ...
							'HDF5 data (*.h5, *.hdf5, *.he5, *.hdf)'; ...
							'*.*', 'All files (*.*)' }, ...
						  'Select Zygo data file');
		assert(~isa(p, 'double'), 'You chose to cancel.');
		full_fn = [p, f{1}];
		fn = f{1};
	end
	if (length(full_fn) > 1)
		[msgs, z] = evalc('load_zygo(full_fn)');
		msgs = [sprintf('Loading file %s...', fn), strsplit(msgs, newline)];
		msgs(cellfun(@(v) strlength(v) < 2, msgs)) = [];
	end
	% so units have been removed in the new ui-framework thingy.. not great.
	% anywho, lets start with a new figure
	f = figure('Name', ['Zygo data inspector - ', fn], ...
			   'NumberTitle', 'off', ...
			   'ToolBar', 'figure', ...
			   'DeleteFcn', @figure_delete_cb, ...
			   'Units', 'normalized', ...
			   'Position', [0, 0, 1, 1]);
	% people are used to jet so lets go with that...
	colormap(f, 'jet');
	
	% left column
	sel_w = 1/4;
	% data set list
	lp = uipanel('Parent', f, ...
				 'Title', 'Data sets', ...
				 'Units', 'normalized', ...
				 'Position', [0, 3/4, sel_w, 1/4]);
	ll = uicontrol('Parent', lp, ...
				   'Style', 'listbox', ...
				   'Callback', @row_sel_cb, ...
				   'String', z.links.name, ...
				   'Units', 'normalized', ...
				   'Position', [0, 0, 1, 1]);
	% path list
	pp = uipanel('Parent', f, ...
				 'Title', 'Paths', ...
				 'Units', 'normalized', ...
				 'Position', [0, 1/2, sel_w, 1/4]);
	pl = uicontrol('Parent', pp, ...
				   'Style', 'listbox', ...
				   'Callback', @row_sel_cb, ...
				   'String', z.path(z.read), ...
				   'Units', 'normalized', ...
				   'Position', [0, 0, 1, 1]);
	% set attribute table
	sp = uipanel('Parent', f, ...
				 'Title', 'Selected set', ...
				 'Units', 'normalized', ...
				 'Position', [0, 1/4, sel_w, 1/4]);
	st = uitable('Parent', sp, ...
				 'RowName', [], ...
				 'ColumnName', {'Attribute', 'Value'}, ...
				 'Units', 'normalized', ...
				 'Position', [0, 0, 1, 1]);
	% data manipulation tabs
	mp = uipanel('Parent', f, ...
				 'Title', 'Set manipulation', ...
				 'Units', 'normalized', ...
				 'Position', [0, 1/8, sel_w, 1/8]);
	mtg = uitabgroup(mp, 'Units', 'normalized', 'Position', [0 0 1 1]);
	% interpolation tab
	mt_sc_int = uitab(mtg, 'Title', 'Interpolate');
	uicontrol('Parent', mt_sc_int, ...
			  'Style', 'text', ...
			  'String', 'Interpolation:', ...
			  'Units', 'normalized', ...
			  'Position', [0, 2/3, 1/4, 1/3]);
	sc_int_int_dd = uicontrol('Parent', mt_sc_int, 'Style', 'popupmenu', ...
							  'String', { 'Linear', ...
										  'Nearest neighbor', ...
										  'Natural neighbor' }, ...
							  'Units', 'normalized', ...
							  'Position', [1/4, 2/3, 3/4, 1/3]);
	uicontrol('Parent', mt_sc_int, ...
			  'Style', 'text', ...
			  'String', 'Extrapolation:', ...
			  'Units', 'normalized', ...
			  'Position', [0, 1/3, 1/4, 1/3]);
	sc_int_ext_dd = uicontrol('Parent', mt_sc_int, 'Style', 'popupmenu', ...
							  'String', { 'Linear', ...
										  'Nearest neighbor', ...
										  'None' }, ...
							  'Units', 'normalized', ...
							  'Position', [1/4, 1/3, 3/4, 1/3]);
	uicontrol('Parent', mt_sc_int, 'Style', 'pushbutton', 'String', 'Apply', ...
			  'Callback', @(~,~) sc_int_cb(sc_int_int_dd.Value, ...
										   sc_int_ext_dd.Value), ...
			  'Units', 'normalized', ...
			  'Position', [3/4, 0, 1/4, 1/3]);
	% norming tab
	mt_norm = uitab(mtg, 'Title', 'Normalize');
	uicontrol('Parent', mt_norm, ...
			  'Style', 'text', ...
			  'String', 'Method:', ...
			  'Units', 'normalized', ...
			  'Position', [0, 2/3, 1/4, 1/3]);
	norm_dd = uicontrol('Parent', mt_norm, 'Style', 'popupmenu', ...
						'String', { 'mean norm', ...
									'abs-max norm', ...
									'0 - 1 mapping', ...
									'-1 - 1 mapping', ...
									'standard score' }, ...
						'Units', 'normalized', ...
						'Position', [1/4, 2/3, 3/4, 1/3], ...
						'Callback', @(c, e) norm_dd_cb(c.Value));
	uicontrol('Parent', mt_norm, ...
			  'Style', 'text', ...
			  'String', 'Equation:', ...
			  'Units', 'normalized', ...
			  'Position', [0, 1/3, 1/4, 1/3]);
	norm_eq = html_label(mt_norm, 'normalized', [1/4, 1/3, 3/4, 1/3], ...
						 ' (X - &mu;) / (X<sub>max</sub> - X<sub>min</sub>)');
	% adding this puppy here since it in many langs could've been inlined
	function norm_dd_cb(i)
		eqs = { ' (X - &mu;) / (X<sub>max</sub> - X<sub>min</sub>)', ...
				' X / |X|<sub>max</sub>', ...
				' (X - X<sub>min</sub>) / (X<sub>max</sub> - X<sub>min</sub>)', ...
				' -1 + 2(X - X<sub>min</sub>) / (X<sub>max</sub> - X<sub>min</sub>)', ...
				' (X - &mu;) / &sigma;' };
		norm_eq.setText(sprintf('<html>%s</html>', eqs{i}));
	end
	uicontrol('Parent', mt_norm, 'Style', 'pushbutton', 'String', 'Apply', ...
			  'Callback', @(~,~) norm_cb(norm_dd.Value), ...
			  'Units', 'normalized', ...
			  'Position', [3/4, 0, 1/4, 1/3]);

	% log list
	lp = uipanel('Parent', f, ...
				 'Title', 'Log', ...
				 'Units', 'normalized', ...
				 'Position', [0, 0, sel_w, 1/8]);
	log = uicontrol('Parent', lp, ...
					'Style', 'listbox', ...
					'Enable', 'inactive', ...
					'FontName', 'FixedWidth', ...
					'FontSize', 8, ...
					'String', msgs, ...
					'Max', 2, ...
					'Value', [], ...
					'Units', 'normalized', ...
					'ListboxTop', numel(msgs), ...
					'Position', [0, 0, 1, 1]);

	log_i = log_fun('Building UI... ', 0);
	
	% center column
	ah = axes('Parent', f, ...
			  'XTick', [], 'YTick', [], ...
			  'Units', 'normalized', ...
			  'OuterPosition', [sel_w, 0.05, 1-2*sel_w, 0.9]);
	colorbar(ah);
 	%cb = colorbar(ah);
% 	f.ResizeFcn = @fig_resize_cb;
	% context menu for the surface viewers
	img_cm = uicontextmenu(f);
	uimenu('Parent', img_cm, ...
		   'Label', 'Export surface data to workspace', ...
		   'Callback', @(~,~) save_surf_cb('ws'));
	uimenu('Parent', img_cm, ...
		   'Label', 'Save surface data to file', ...
		   'Callback', @(~,~) save_surf_cb('file'));
	cm_m = uimenu('Parent', img_cm, ...
				  'Label', 'Select colormap', ...
				  'Separator', 'on');
	% since there doesn't seem to be a (non-hacky) programmatical way to get the
	% available colormaps i just hardcoded it and if it fails here we'll disable
	% the menu item (done in the callback)
	cms = { 'jet', 'parula', 'hsv', 'hot', 'cool', 'spring', 'summer', ...
			'autumn', 'winter', 'gray', 'bone', 'copper', 'pink' };
	uimenu('Parent', cm_m, 'Label', cms{1}, 'Callback', @cmap_sel_cb, ...
		   'Checked', 'on');
	for cm_i = 2 : numel(cms)
		uimenu('Parent', cm_m, 'Label', cms{cm_i}, 'Callback', @cmap_sel_cb);
	end
	% "loading screen"
	loading_img_dat = flipud([ ...
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
		1 0 1 1 1 1 1 0 0 1 1 1 1 1 0 1 1 1 0 0 1 1 1 0 1 0 1 1 1 0 1 1 1 0 0 1 1 1 1 1 1 1 1
		1 0 1 1 1 1 0 1 1 0 1 1 1 0 1 0 1 1 0 1 0 1 1 0 1 0 0 1 1 0 1 1 0 1 1 0 1 1 1 1 1 1 1
		1 0 1 1 1 1 0 1 1 0 1 1 0 1 1 1 0 1 0 1 0 1 1 0 1 0 0 1 1 0 1 0 1 1 1 1 0 1 1 1 1 1 1
		1 0 1 1 1 0 1 1 1 1 0 1 0 1 1 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 1 1 1 1 1 1 1 1 1 1
		1 0 1 1 1 0 1 1 1 1 0 1 0 1 1 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 1 1 1 1 1 1 1 1 1 1
		1 0 1 1 1 0 1 1 1 1 0 1 0 0 0 0 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 1 1 1 1 1 1 1 1 1 1
		1 0 1 1 1 0 1 1 1 1 0 1 0 1 1 1 0 1 0 1 1 0 1 0 1 0 1 0 1 0 1 0 1 1 0 0 0 1 1 1 1 1 1
		1 0 1 1 1 1 0 1 1 0 1 1 0 1 1 1 0 1 0 1 0 1 1 0 1 0 1 1 0 0 1 0 1 1 1 1 0 1 1 1 1 1 1
		1 0 1 1 1 1 0 1 1 0 1 1 0 1 1 1 0 1 0 1 0 1 1 0 1 0 1 1 0 0 1 1 0 1 1 0 1 1 1 1 1 1 1
		1 0 0 0 0 1 1 0 0 1 1 1 0 1 1 1 0 1 0 0 1 1 1 0 1 0 1 1 1 0 1 1 1 0 0 1 1 0 1 0 1 0 1
		1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1
	]);
	img = imagesc(ah, 'UIContextMenu', img_cm, 'CData', loading_img_dat);
	axis(ah, 'xy', 'image');
	
	% right column
	sh = axes('Parent', f, ...
			  'Units', 'normalized', ...
			  'OuterPosition', [1-sel_w, 1/2, sel_w, 1/2]);
	% we'll reuse the context menu from the main view
	s3dh = surf(handle(sh), loading_img_dat, ...
				'LineStyle', 'none', ...
				'UIContextMenu', img_cm);
	lh = axes('Parent', f, ...
			  'Units', 'normalized', ...
			  'OuterPosition', [1-sel_w, 1/8, sel_w, 3/8]);
	xlabel(lh, [char(956), 'm']);
	hold(lh, 'on');
	% check https://se.mathworks.com/matlabcentral/fileexchange/42284-drag-line-in-gui
	% for dragable lines
	lp = uipanel('Parent', f, ...
				 'Title', 'Line Props', ...
				 'Units', 'normalized', ...
				 'Position', [1-sel_w, 0, sel_w, 1/8]);
	% line table context menu
	lt_cm = uicontextmenu(f);
	uimenu('Parent', lt_cm, ...
		   'Label', 'Export selected line(s) to workspace', ...
		   'Callback', @(~,~) save_lines_cb(0, 'ws'));
	uimenu('Parent', lt_cm, ...
		   'Label', 'Export all lines to workspace', ...
		   'Callback', @(~,~) save_lines_cb(1, 'ws'));
	uimenu('Parent', lt_cm, ...
		   'Label', 'Save selected line(s) to file', ...
		   'Separator', 'on', ...
		   'Callback', @(~,~) save_lines_cb(0, 'file'));
	uimenu('Parent', lt_cm, ...
		   'Label', 'Save all lines to file', ...
		   'Callback', @(~,~) save_lines_cb(1, 'file'));
	lt = uitable('Parent', lp, ...
				 'ColumnName', { 'Xs', ...
								 [char(916), 'X'], ...
								 'Ys', ...
								 [char(916), 'Y'], ...
								 'Length', ...
								 'Zs', ...
								 [char(916), 'Z'], ...
								 'Color' }, ...
				 'CellSelectionCallback', @highlight_uitable_row, ...
				 'UIContextMenu', lt_cm, ...
				 'Units', 'normalized', ...
				 'Position', [0, 0, 1, 1]);
	lines = {};
	l_int = gobjects(0);
	% TODO add Z-converters..
	convs = struct('x', 1, 'y', 1, 'scale', 1, 'unit', 'm');
	dat_nans = [];
	
	% add the lateral units and add/delete buttons to the figure toolbar, since
	% this relies on everything having been rendered (i.e. the java components
	% are created) we have to wait here
	drawnow(); pause(0.1);
	tbwrn = { 'Button Creation Failed', 'modal' };
	tb = findall(f, 'tag', 'FigureToolBar');
	if (isempty(tb))
		warndlg(sprintf(['Failed to access the figure toolbar.', ...
						 'Lateral units will be set to micrometers and ', ...
						 'only one line.']), ...
				tbwrn{:});
	else
		% add and delete line buttons
		b = toolbar_text_btn(tb, @uipushtool, @add_line_cb, ...
							 'Add line', 'Add new line', ...
							 'Separator', 'on');
		if (isempty(b))
			warndlg(sprintf(['Failed to create the add line button.\n', ...
							 'Only one line will be available.']), ...
					tbwrn{:});
		end
		b = toolbar_text_btn(tb, @uipushtool, @del_line_cb, ...
							 'Delete line', 'Delete last created line', ...
							 'Separator', 'on');
		if (isempty(b))
			warndlg(sprintf(['Failed to create the delete line button.\n', ...
							 'You can now only add new lines.']), ...
					tbwrn{:});
		end
		% add a separator (doesn't matter if it works really)
		toolbar_separator(tb);
		% lateral unit label and dropdown
		toolbar_label(tb, ' Lateral units: ', 'Lateral units in all graphics');
		dd = toolbar_dropdown(tb, @lateral_unit_cb, ...
							  { 'Pixels (px)', 'Nanometers (nm)', ...
								['Micrometers (', char(956), 'm)'], ...
								'Millimeters (mm)' }, 3);
		if (isempty(dd))
			warndlg(sprintf(['Failed to create the lateral unit dropdown.\n', ...
							 'Lateral units will be set to micrometers.']), ...
					tbwrn{:});
		end
	end
	
	% hopefully everything is done now, so let's resize the figure again
	drawnow(); pause(0.1);
	set(f, 'OuterPosition', [0, 0, 1, 1]);
	log_fun('OK!', 0, log_i);
	log_i = log_fun('Plotting data... ', 0);
	% select Surface if available, otherwise just first
	s_idx = find(strcmp(z.links.name, 'Surface'), 1);
	if (isempty(s_idx)); s_idx = 1; end
	ll.Value = s_idx;
	% setting Value doesn't trigger the callback, so we need to do it manually
	row_sel_cb(ll, 0);
	% let's also add a line to begin with
	add_line_cb(0, 0);
	drawnow();
	log_fun('OK!', 0, log_i);
	
	%% ui-element callbacks
	function row_sel_cb(s, ~)
		% cb for when a row in either the links- or paths-table is selected
		if (s == pl)
			idx = find(z.links.idx == s.Value, 1);
			if (~isempty(idx)); ll.Value = z.links.idx(idx);
			else % very ugly hack to deselect all
				ll.Max = 2;
				ll.Value = [];
			end
		elseif (s == ll)
			if (s.Max > 1)
				s.Value = s.Value(1);
				s.Max = 1;
			end
			pl.Value = z.links.idx(s.Value);
		else
			errordlg('Listbox callback called by unknown uicontrol.', ...
					 'UI error', 'modal');
		end
		li = log_fun(sprintf('Loading %s... ', pl.String{pl.Value}), 0);
		% parse the attributes
		atts = z.attributes{pl.Value};
		dat = z.set{pl.Value};
		nan_idx = [];
		unit_idx = [];
		if (size(atts, 1) > 0)
			sav = cell(size(atts, 1), 1);
			for i = 1 : numel(sav)
				sav{i} = any2str(atts{i, 2});
			end
			cw = 6 + 2*(isunix() && ~ismac());
			set(st, 'Data', [atts(:, 1), sav], ...
					'ColumnWidth', { max(strlength(atts(:, 1)))*cw, ...
									 max(strlength(sav)).*cw });
			nan_idx = find(strcmp(atts(:,1), 'No Data'), 1);
			unit_idx = find(strcmp(atts(:,1), 'Unit'), 1);
			convs.x = get_conv_factor(atts, 'X Converter');
			convs.y = get_conv_factor(atts, 'Y Converter');
		else
			set(st, 'Data', []);
		end
		% ensure we got some data
		if (min(size(dat)) == 0)
			warndlg(['The selected data set is empty, ', ...
					 'no visualization possible.'], ...
					'Empty set', ...
					'modal');
			log_fun('OK (empty set)', 0, li);
			return;
		end
		% if it's a vector just show it in a new figure
		if (min(size(dat)) == 1)
			helpdlg(sprintf(['The selected data set only contains a vector.\n', ...
							 'To visualize it a new figure will be opened.']), ...
					'Vector data');
			sf = figure('Name', 'Zygo data inspector - vector data', ...
						'NumberTitle', 'off');
			sah = axes('Parent', sf);
			if (~isempty(nan_idx)); dat(dat == atts{nan_idx, 2}) = nan; end
			plot(sah, dat);
			log_fun('OK (vector set)', 0, li);
			return;
		end
		% if it's proper-sized we continue
		if (~isempty(nan_idx)); nan_val = atts{nan_idx, 2};
		else; nan_val = max(dat(:));
		end
		if (~isempty(unit_idx))
			title(ah, sprintf('Z unit: %s', atts{unit_idx, 2}));
			zlabel(sh, atts{unit_idx, 2});
			ylabel(lh, atts{unit_idx, 2});
		else
			title(ah, 'Z unit: unknown');
		end 
		dat_nans = (dat == nan_val);
		dat(dat_nans) = nan;
		log_fun('OK!', 0, li);
		show_img(dat);
	end

	function sc_int_cb(interp_i, extrap_i)
		% data manipulation > scatterInterpolant callback
		interp_methods = {'linear', 'nearest', 'natural'};
		extrap_methods = {'linear', 'nearest', 'none'};
		li = log_fun(sprintf('Interpolating values using %s/%s... ', ...
							 interp_methods{interp_i}, ...
							 extrap_methods{extrap_i}), 0);
		img_dat = get(img, 'CData');
		[r, c] = find(~dat_nans);
		F = scatteredInterpolant([r, c], img_dat(~dat_nans), ...
								 interp_methods{interp_i}, ...
								 extrap_methods{extrap_i});
		[qr, qc] = find(dat_nans);
		img_dat(dat_nans) = F(qr, qc);
		log_fun('OK!', 0, li);
		show_img(img_dat);
	end

	function norm_cb(method)
		li = log_fun('Normalizing values.. ', 0);
		switch (method)
			case 1 % meannorm
				dat_fn = @(x) (x - mean(x, 'all', 'omitnan')) ./ ...
							  (max(x, [], 'all') - min(x, [], 'all'));
			case 2 % maxdiv
				dat_fn = @(x) x ./ max(abs(x), [], 'all');
			case 3 % 01map
				dat_fn = @(x) (x - min(x, [], 'all')) ./ ...
							  (max(x, [], 'all') - min(x, [], 'all'));
			case 4 % -11map
				dat_fn = @(x) -1 + 2*(x - min(x, [], 'all')) ./ ...
								  (max(x, [], 'all') - min(x, [], 'all'));
			case 5 % meanstd (standard score)
				dat_fn = @(x) (x - mean(x, 'all', 'omitnan')) ./ ...
							   std(x, 1, 'all', 'omitnan');
			otherwise
				return
		end
		method_name = norm_dd.String{method};
		show_img(dat_fn(get(img, 'CData')));
		title(ah, sprintf('Z unit: %s', method_name));
		zlabel(sh, method_name);
		ylabel(lh, method_name);
		log_fun('OK!', 0, li);
	end
		
	function add_line_cb(~, ~)
		% add line button cb
		clrs = ['k', 'b', 'm', 'r', 'g', 'y', 'c'];
		clr = clrs(mod(numel(l_int), numel(clrs))+1);
		dat = get(img, 'CData');
		[y, x] = size(dat);
		% pos is a 2x2 matrix, just like in the callback (remember: in matlab
		% single indices in matrices goes "depth first")
		n_lns = mod(numel(lines), 4) + 1;
		l_pos = round([x/4, y*n_lns/5; x*3/4, y*n_lns/5]);
		% So imline is not recommended anymore but doing the stuff with the new
		% functions was just a chore so i'll just keep this.
		lines{end+1} = imline(ah, l_pos); %#ok<IMLINE>
		setColor(lines{end}, clr);
		lines{end}.Deletable = false; % fails when being set as name-value
		addNewPositionCallback(lines{end}, @(p) line_pos_cb(p, numel(lines)));
		c = improfile(double(dat), l_pos(:, 1)', l_pos(:, 2)');
		[Xs, dX, Ys, dY, l_len] = pos_conv(l_pos);
		l_int(end+1) = plot(lh, linspace(0, l_len, length(c)), c, 'Color', clr);
		zs = [min(c), max(c)];
		lt_dat = { Xs, dX, Ys, dY, l_len, ...
				   sprintf('%g, %g', zs(1), zs(2)), zs(2)-zs(1), ...
				   clr2html(l_int(end).Color) };
		% addStyle/uistyle only works for uifigure-shenanigans..
		if (isempty(lt.Data)); lt.Data = lt_dat;
		else; lt.Data(end+1, :) = lt_dat;
		end
		log_fun(sprintf('Added line %u', numel(l_int)), 0);
	end

	function del_line_cb(~, ~)
		% delete line button cb
		if (numel(lines) > 0)
			delete(lines{end});
			delete(l_int(end));
			lines(end) = [];
			l_int(end) = [];
			lt.Data(end, :) = [];
			log_fun(sprintf('Line %u deleted', numel(l_int)+1), 0);
		else
			log_fun('No lines left, can''t delete.', 1);
		end
	end

	function lateral_unit_cb(dd, ~)
		% lateral units dropdown menu selection cb
		switch (get(dd, 'SelectedIndex'))
			case 0
				convs.scale = -1;
				convs.unit = 'px';
			case 1
				convs.scale = 1e9;
				convs.unit = 'nm';
			case 2
				convs.scale = 1e6;
				convs.unit = [char(956), 'm'];
			case 3
				convs.scale = 1e3;
				convs.unit = 'mm';
			otherwise
				warndlg('Invalid lateral unit selection!', ...
						'Unit selection', ...
						'modal');
				return;
		end
		update_line_dat(false, true);
		log_fun(sprintf('Lateral units set to %s', convs.unit), 0);
	end

	function line_pos_cb(pos, l_idx)
		% line position change (translation or end movement) cb
		c = improfile(double(get(img, 'CData')), pos(:, 1)', pos(:, 2)');
		[Xs, dX, Ys, dY, l_len] = pos_conv(pos);
		set(l_int(l_idx), 'XData', linspace(0, l_len, length(c)), 'YData', c);
		zs = [min(c), max(c)];
		lt.Data(l_idx, 1:end-1) = { Xs, dX, Ys, dY, l_len, ...
									sprintf('%g, %g', zs(1), zs(2)), ...
									zs(2)-zs(1) };
	end

	function cmap_sel_cb(mnu, ~)
		% select colormap context menu item cb
		siblings = get(get(mnu, 'Parent'), 'Children');
		li = log_fun(sprintf('Setting colormap to %s... ', mnu.Text), 0);
		for mnu_i = 1 : numel(siblings)
			% this is just in case the colormap-call fails
			if (siblings(mnu_i).Checked(end) == 'n')
				old_sel = siblings(mnu_i);
			end
			siblings(mnu_i).Checked = 'off';
		end
		try
			colormap(f, mnu.Text);
			mnu.Checked = 'on';
			log_fun('OK!', 0, li);
		catch e
			errordlg(sprintf('Setting the colormap failed:\n%s', e.message), ...
					 'Colormap error', 'modal');
			old_sel.Checked = 'on';
			mnu.Enable = 'off';
			log_fun('Error!', 2, li);
		end
	end

	function save_surf_cb(target)
		log_fun('Exporting surface data', 0);
		export_dat(target, 'surface', ...
				   struct('z_data', get(img, 'CData'), ...
						  'z_data_unit', get(get(lh, 'YLabel'), 'String'), ...
						  'x2meters', convs.x, 'y2meters', convs.y));
	end

	function save_lines_cb(sel_or_all, target)
		% line table context menu cb
		if (sel_or_all == 0) % 0 means selected lines
			if (isempty(lt.UserData))
				warndlg('No lines have been selected!', 'No selection', ...
						'modal');
				return;
			else
				lns = sort(lt.UserData);
				log_fun(sprintf('Exporting lines %s', ...
								strjoin(sprintfc('%u', lns), ', ')), 0);
			end
		else
			lns = 1:length(l_int);
			log_fun('Exporting all lines', 0);
		end
		% TODO: make sure that ylabel works in older versions of matlab (or just
		% make a better version of it that loads the attribute)
		ln_dat = repmat(struct('index', 0, 'p1_px', [], 'p2_px', [], ...
							   'x2meters', convs.x, 'y2meters', convs.y, ...
							   'z_data', [], 'z_data_unit', ...
							   get(get(lh, 'YLabel'), 'String')), ...
						length(lns), 1);
		c = 1;
		for ln_i = lns
			ln_pos = getPosition(lines{ln_i});
			ln_dat(c).index = ln_i;
			ln_dat(c).p1_px = ln_pos(1, :);
			ln_dat(c).p2_px = ln_pos(2, :);
			ln_dat(c).z_data = l_int(ln_i).YData;
			c = c + 1;
		end
		export_dat(target, 'line', ln_dat);
	end

% 	function fig_resize_cb(f, ~)
% 		fp = getpixelposition(f);
% 		[pb_ar, szs] = get_axes_ar(ah, cb);
% 		get(ah, 'Position');
% 		%ah, cb
% 	end

	function figure_delete_cb(~, ~)
		log_timer = timerfind('Name', 'zygo_log_hl_timer');
		if (~isempty(log_timer))
			stop(log_timer);
			delete(log_timer);
		end
	end

	%% utility functions
	function log_idx = log_fun(str, lvl, varargin)
		persistent hl_timer;
		function remove_log_hl_cb(~, ~)
			log.Value = [];
		end
		if (isempty(hl_timer) || ~isvalid(hl_timer))
			hl_timer = timer('Name', 'zygo_log_hl_timer', ...
							 'StartDelay', 3, ...
							 'TimerFcn', @remove_log_hl_cb);
		else % stopping a non-started timer does nothing
			stop(hl_timer);
		end
		switch(lvl)
			case 1
				str = sprintf('<html><font color="#aaaa00"><b>%s</b></font></html>', ...
							  str);
			case 2
				str = sprintf('<html><font color="#ff0000"><b>%s</b></font></html>', ...
							  str);
		end
		if (nargin == 2)
			log.String{end+1} = str;
			% unfortunately SelectionHighlight has been removed since 2014 so
			% we'll have to live with the last one being selected for now (since
			% ListboxTop doesn't seem to work properly..)
			log.Value = numel(log.String);
			log_idx = numel(log.String);
		else
			log.String{varargin{1}} = [log.String{varargin{1}}, str];
			log_idx = varargin{1};
		end
		start(hl_timer);
		drawnow();
	end

	function export_dat(target, dat_name, zygo_data)
		if (strcmp(target, 'ws'))
			var_name = inputdlg({[ 'Variable name to export ', ...
								   dat_name, ' data to:' ]}, ...
								'Export settings', ...
								[1 40], ...
								{'zygo_data'});
			if (isempty(var_name))
				log_fun('Exporting canceled by user', 0);
				return;
			end
			try
				assignin('base', var_name{1}, zygo_data);
			catch e
				errordlg(sprintf('Exporting to workspace failed:\n%s', ...
								 e.message), ...
						 'Export error', 'modal');
				log_fun('Export failed', 2);
			end
		else
			[of, op] = file_dlg(@uiputfile, '*.mat', ...
								['Save ', dat_name, ' data to file']);
			if (isa(op, 'double'))
				log_fun('Exporting canceled by user', 0);
				return;
			end
			save([op, of{1}], 'zygo_data');
		end
	end
	
	function show_img(dat)
		li = log_fun('Plotting new surface... ', 0);
		set(img, 'CData', dat, 'AlphaData', ~isnan(dat));
		w = size(dat, 2);
		h = size(dat, 1);
		sx = linspace(0, w, w);
		sy = linspace(0, h, h);
		if (convs.scale > -1)
			w = w * convs.x * convs.scale;
			h = h * convs.y * convs.scale;
			sx = sx .* convs.x * convs.scale;
			sy = sy .* convs.y * convs.scale;
		end
		xlabel(ah, sprintf('%.1f x %.1f %s', w, h, convs.unit));
		set(s3dh, 'XData', sx, 'YData', sy, 'ZData', dat);
		view(sh, -45, 45);
		% the since the position of the 
		update_line_dat(true, false);
 		drawnow();
		xlabel(sh, sprintf('width [%s]', convs.unit));
		ylabel(sh, sprintf('height [%s]', convs.unit));
		log_fun('OK!', 0, li);
	end

	function [Xs, dX, Ys, dY, l_len] = pos_conv(pos)
		dX = abs(pos(2) - pos(1));
		dY = abs(pos(4) - pos(3));
		if (convs.scale > -1)
			dX = dX * convs.scale * convs.x;
			dY = dY * convs.scale * convs.y;
			pos(1:2) = pos(1:2) .* convs.scale * convs.x;
			pos(3:4) = pos(3:4) .* convs.scale * convs.y;
			Xs = sprintf('%.1f, %.1f', pos(1), pos(2));
			Ys = sprintf('%.1f, %.1f', pos(3), pos(4));
		else
			pos = round(pos);
			Xs = sprintf('%u, %u', pos(1), pos(2));
			Ys = sprintf('%u, %u', pos(3), pos(4));
		end
		l_len = sqrt(dX^2 + dY^2);
	end

	function update_line_dat(intensity, xdat_or_labels)
		if (intensity)
			dat = double(get(img, 'CData'));
		elseif (~xdat_or_labels)
			% if neither the labels nor the intensity needs updating there's
			% really nothing to do here since the lines' X- and YData should be
			% the same (this func is not called when a line is moved)
			return;
		end
		lt_dat = lt.Data;
		for i = 1 : numel(lines)
			if (xdat_or_labels)
				[lt_dat{i, 1:5}] = pos_conv(getPosition(lines{i}));
				set(l_int(i), 'XData', ...
					linspace(0, lt_dat{i, 5}, ...
							 length(get(l_int(i), 'XData'))));
			end
			if (intensity)
				% intensity is kinda costly to update
				l_pos = getPosition(lines{i});
				c = improfile(dat, l_pos(:, 1)', l_pos(:, 2)');
				set(l_int(i), 'YData', c);
				zs = [min(c), max(c)];
				lt_dat(i, 6:7) = { sprintf('%g, %g', zs(1), zs(2)), ...
								   zs(2)-zs(1) };
			end
		end
		if (xdat_or_labels)
			% updating labels requires updating X- and YData in the plots, the
			% line ones have been updated above in the loop so we'll just set
			% the labels here
			xlabel(lh, sprintf('line [%s]', convs.unit));
			% for the 3D-plot we need new X- and YData tho
			if (intensity); img_sz = size(dat);
			else; img_sz = size(get(img, 'CData'));
			end
			sx = linspace(0, img_sz(2), img_sz(2));
			sy = linspace(0, img_sz(1), img_sz(1));
			if (convs.scale > -1)
				sx = sx .* convs.x * convs.scale;
				sy = sy .* convs.y * convs.scale;
				% lets update the main image label here so we don't have to do
				% the same check later
				xlabel(ah, sprintf('%.1f x %.1f %s', ...
								   convs.scale * convs.x * img_sz(2), ...
								   convs.scale * convs.y * img_sz(1), ...
								   convs.unit));
			else
				xlabel(lh, 'line [px]');
			end
			% the 3D-plot _should_ only have the one child
			set(s3dh, 'XData', sx, 'YData', sy);
			xlabel(sh, sprintf('width [%s]', convs.unit));
			ylabel(sh, sprintf('height [%s]', convs.unit));
		end
		% finally update the actual table data
		lt.Data = lt_dat;
	end
end

%% HDF5-related functions
function f = get_conv_factor(atts, s)
	f = 1;
	conv_idx = find(strcmp(atts(:,1), s), 1);
	if (isempty(conv_idx)); return;
	else; conv = atts{conv_idx, 2};
	end
	if (isstruct(conv) && isfield(conv, 'Parameters'))
		% TODO: verify that this is actually the one we're interested in
		f = conv.Parameters{1}(2);
	end
end

%% utility (mainly ui)functions that has nothing to do with the zygo-stuff
function s = any2str(v)
	% this puppy will crash and burn if v is a struct/cell with
	% substruct(s)/cell(s) with substruct(s)/cell(s) and so on deeper than the
	% matlab recursion limit (get(0, 'RecursionLimit'), default value 500).
	vec2str = @(x) ['[', strjoin(sprintfc('%g', x), ' '), ']'];
	if (isempty(v))
		s = '[]';
	elseif (ischar(v))
		s = ['''', v, ''''];
	elseif (~verLessThan('matlab', '9.1') && isstring(v))
		s = ['"', char(strjoin(v, '", "')), '"']; % fucking strings
	elseif (isnumeric(v))
		if (numel(v) > 1); s = vec2str(v);
		else; s = sprintf('%g', v);
		end
	elseif (islogical(v))
		tmp = {'false', 'true'};
		s = tmp{v + 1};
	elseif (isstruct(v))
		flds = fieldnames(v);
		tmp = cell(numel(flds), 1);
		for i = 1 : numel(flds)
			tmp{i} = [flds{i}, ': ', any2str(v.(flds{i}))];
		end
		s = ['(', strjoin(tmp, ', '), ')'];
	elseif (iscellstr(v))
		s = ['''', strjoin(v, ''', '''), ''''];
	elseif (iscell(v))
		if (numel(v) > 1)
			tmp = cell(numel(v), 1);
			for i = 1 : numel(v)
				tmp{i} = any2str(v{i});
			end
			s = ['{', strjoin(tmp, ', '), '}'];
		else
			s = any2str(v{1});
		end
	else
		s = ['class = ', class(v)];
	end
end

function [f, p] = file_dlg(ui_dlg_fn, varargin)
	% uiget/putfile doesn't "remember" last folder and returns different data
	% types for f depending on if it's a single or multiple files that has been
	% selected, hence this wrapper
	persistent sp;
	cdtosp = (ischar(sp) && isfolder(sp));
	if (cdtosp)
		currdir = pwd();
		cd(sp);
	end
	[f, p] = ui_dlg_fn(varargin{:});
	if (cdtosp); cd(currdir); end
	if (isa(f, 'char')); f = {f}; end
	if (isa(p, 'char')); sp = p; end
end

function highlight_uitable_row(tbl, e)
	if (size(e.Indices, 1) < 1); return; end
	if (ispc()); hlclr = [0.4 0.7 1]; % TODO: check win hl color
	elseif (ismac()); hlclr = [0.549 0.4745 0.6169];
	else; hlclr = [0.4 0.7 1];
	end
	bg = ones(size(tbl.Data, 1), 3); % defaults to white
	for i = 1 : size(bg, 1)
		if (any(i == e.Indices(:, 1))) % "multirow selection support"
			bg(i, :) = hlclr;
		elseif (mod(i, 2) == 0)
			bg(i, :) = [0.94 0.94 0.94];
		end
	end
	tbl.BackgroundColor = bg;
	tbl.UserData = e.Indices(:, 1)'; % we'll use UserData for current selection
end

function s = clr2html(clr)
	% TODO: do this better...
	clr = round(clr .* 255);
	s = sprintf(['<html><body style="background-color:#%02x%02x%02x; ', ...
									'width:100%%; ', ...
									'height:100%%;">', ...
					'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', ...
				'</body></html>'], ...
				clr(1), clr(2), clr(3));
end

function jlbl = html_label(parent, units, pos, str)
	if (~verLessThan('MATLAB', '9.7'))
		warning('off', 'MATLAB:ui:javacomponent:FunctionToBeRemoved');
	end
	jlbl = javaObjectEDT('javax.swing.JLabel', sprintf('<html>%s</html>', str));
	[~, lbl] = javacomponent(jlbl, [0, 0, 1, 1], parent); %#ok<*JAVCM> 
	lbl.Units = units;
	lbl.Position = pos;
	if (~verLessThan('MATLAB', '9.7'))
		warning('on', 'MATLAB:ui:javacomponent:FunctionToBeRemoved');
	end
end

function b = toolbar_text_btn(tb, uicall, cb, str, tooltip, varargin)
	% ref: https://undocumentedmatlab.com/blog/toolbar-button-labels
	b = [];
	jtb = get(get(tb, 'JavaContainer'), 'ComponentPeer'); %#ok<*JAVCT> 
	if (isempty(jtb)); return; end
	b = uicall(tb, 'ClickedCallback', cb, ...
				   'TooltipString', tooltip, varargin{:});
	drawnow(); pause(0.1); % pause for java component creation
	jb = jtb.getComponent(jtb.getComponentCount() - 1);
	gfx = jb.getGraphics();
	w = gfx.getFontMetrics().getStringBounds(str, gfx).getWidth() + 16;
	sz = java.awt.Dimension(w, jb.getHeight());
	jb.setMaximumSize(sz);
	jb.setPreferredSize(sz);
	jb.setSize(sz);
	jb.setText(str);
end

function jdd = toolbar_dropdown(tb, cb, opts, si)
	% args: toolbar, callback, options_for_dropdown, selected_index (1-based)
	% ref: https://undocumentedmatlab.com/blog/figure-toolbar-components
	jdd = javax.swing.JComboBox(opts);
	set(jdd, 'ActionPerformedCallback', cb, 'SelectedIndex', si-1);
	if (~toolbar_push_component(tb, jdd))
		delete(jlbl);
		jdd = [];
	end
end

function jlbl = toolbar_label(tb, str, tooltip, varargin)
	jlbl = javax.swing.JLabel(str);
	set(jlbl, 'ToolTipText', tooltip, varargin{:});
	if (~toolbar_push_component(tb, jlbl))
		delete(jlbl);
		jlbl = [];
	end
end

function success = toolbar_separator(tb)
	success = toolbar_push_component(tb, '_SEPARATOR');
end

function success = toolbar_push_component(tb, c)
	success = false;
	jtb = get(get(tb, 'JavaContainer'), 'ComponentPeer');
	if (isempty(jtb)); return; end
	if (isa(c, 'char') && strcmp(c, '_SEPARATOR')); jtb(1).addSeparator();
	else; jtb(1).add(c, -1); %, jtb.getComponentCount());
	end
	jtb(1).repaint;
	jtb(1).revalidate;
	success = true;
end

% function [ar, varargout] = get_axes_sz(a, varargin)
% 	% args: axes
% 	% opt args: colorbar
% 	% note: doesn't work for multiple x- or y-axes and doesn't account for
% 	% outward ticks.. should probably fix that.
% 	app = getpixelposition(a);
% 	% colorbars are included in the axes dimensions but the extent of title and
% 	% labels doesn't seem to be..
% 	extent = @(a, l) get(get(a, l), 'Extent');
% 	te = extent(a, 'Title');
% 	ah = app(4) + te(4);
% 	xle = extent(a, 'XLabel');
% 	ah = ah + xle(4);
% 	yle = extent(a, 'YLabel');
% 	aw = app(3) + yle(3);
% 	% calc the adjusted aspect ratio
% 	pos_ar = aw / ah;
% 	pb_scales = pbaspect(ah);
% 	pb_ar = pb_scales(1) / pb_scales(2);
% 	if (pb_ar > pos_ar) % pb is "wider" than axes
% 		w = app(3);
% 		if (nargin > 1)
% 			cbpp = getpixelposition(varargin{1});
% 			w = w - cbpp(3); % this is not actually the full width of colorbar
% 		end
% 		h = w / pb_ar;
% 	else % pb is "higher" than axes
% 		h = app(4);
% 		w = h * pb_ar;
% 	end
% 	ar = (w/h;
% 	if (nargout > 1)
% 		varargout{1} = [w h aw ah];
% 	end
% 	
% % ah = gca();
% % % Get the axes Position rectangle in units of pixels
% % old_units = get(ah,'Units');
% % set(ah,'Units','pixels');
% % pos = get(ah,'Position');
% % set(ah,'Units',old_units);
% % % Figure the PlotBox and axes Position aspect ratios
% % pos_aspectRatio = pos(3) / pos(4);
% % box_aspect = pbaspect(ah);
% % box_aspectRatio = box_aspect(1) / box_aspect(2);
% % if (box_aspectRatio > pos_aspectRatio)
% %     % PlotBox is wider than the Position rectangle
% %     box_height = pos(3) / box_aspectRatio;
% %     box_dy = (pos(4)-box_height) / 2;
% %     box_position = [pos(1), pos(2)+box_dy, pos(3), box_height];
% % else
% %     % PlotBox is taller than the Position rectangle
% %     box_width = pos(4) * box_aspectRatio;
% %     box_dx = (pos(3)-box_width) / 2;
% %     box_position = [pos(1)+box_dx, pos(2), box_width, pos(4)];
% % end
% 	
% end


% addLineIcon = [ ...
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 1 1 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 1 1 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
% 0 0 0 0 0 0 0 1 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0
% 0 0 0 1 1 1 1 0 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0
% 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
% 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
% 0 0 0 1 1 1 1 0 0 0 0 0 0 0 0 0 1 1 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% ];
% delLineIcon = [ ...
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 1 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 1 1 1 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 1 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0
% 0 0 0 1 1 1 1 0 0 0 0 0 0 1 1 1 1 1 1 1 1 0 0 0
% 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 1 0 0 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 1 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0
% ];



