% TODO:
% - fix the loading order, seems to load it "upside down" now

function gd = load_zygo(fn)
	nfo = h5info(fn);
	gd = struct('read', logical([]), 'path', {{}}, 'set', {{}}, ...
				'attributes', {{}}, 'links', struct('name', {{}}, 'idx', []));
	for ii = 1 : length(nfo.Groups)
		gd = read_group(fn, nfo.Groups(ii), gd);
	end
end

function gd = read_group(fn, g, gd)
	for i = 1 : length(g.Groups)
		if (~isempty(g.Groups(i)))
			gd = read_group(fn, g.Groups(i), gd);
		end
	end
	for i = 1 : length(g.Datasets)
		try
			np = [g.Name, '/', g.Datasets(i).Name];
			fprintf('Reading path ''%s''... ', np);
			gd.path{end+1} = np;
			gd.attributes{end+1} = read_attrs(g.Datasets(i).Attributes);
			% the zygo software stores the data rows while matlab wants it in
			% cols (at least the visualization)
			gd.set{end+1} = h5read(fn, np)';
			gd.read(end+1) = true;
			fprintf('OK!\n');
		catch e
			fprintf('FAILED:\n%s\n', e.message);
			gd.set{end+1} = [];
			gd.read(end+1) = false;
		end
	end
	for i = 1 : length(g.Links)
		if (strcmp(g.Links(i).Name, 'Attributes')); continue; end
		idx = find(strcmp(gd.path, g.Links(i).Value), 1);
		fprintf('Adding link %s -> %s... ', g.Links(i).Name, g.Links(i).Value{1});
		if (~isempty(idx))
			gd.links.name{end+1} = g.Links(i).Name;
			gd.links.idx(end+1) = idx;
			fprintf('OK!\n');
		else
			fprintf('FAILED: Path has not been read!\n');
		end
	end
end

function a = read_attrs(attrs)
	a = cell(numel(attrs), 2);
	fprintf('(%d attrs) ', numel(attrs));
	for i = 1 : numel(attrs)
		% hdf5 has a bunch of datatypes, here we'll reduce it to either a
		% numeric (double) matrix or string
		a{i, 1} = attrs(i).Name;
		if (any(strcmpi({'scalar', 'simple'}, attrs(i).Dataspace.Type)))
			switch (attrs(i).Datatype.Class)
				case 'H5T_BITFIELD'
					% TODO convert the integer to logical vector (just gotta
					% find the de2bi that i wrote somewhere..)
					a{i, 2} = attrs(i).Value;
				case 'H5T_ENUM'
					a{i, 2} = attrs(i).Value{1};
				case 'H5T_STRING'
					% TODO check if this works when Dataspace.Type == 'simple'
					% and it's a "cell matrix"
					a{i, 2} = strjoin(attrs(i).Value, '\n');
				otherwise
					a{i, 2} = attrs(i).Value;
			end
		else
			a{i, 2} = [];
		end
	end
end

