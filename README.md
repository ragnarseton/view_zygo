# MATLAB Zygo viewer

Zygo optical profiler data viewer inspired by their Mx-software.

## Description

You can basically open and inspect the datx-files (which are really hdf5-files) the acquisition software produces, if you want to do anything more advanced than simple interpolation or normalization you can export surface or line data to mat-files and/or the current workspace.
Here's the more or less static parts of `help view_zygo`:

###   Zygo data viewer
To open a datafile acquired by the Zygo optical profiler in the cleanroom at UU (i don't really remember the model atm) just call this function without arguments, select the file you want to view and open. You can export line data by right clicking the lower right hand table and surface data by right clicking either the flat or 3D-surface view. Note that the controls to add and delete lines and change lateral units are located in the toolbar just below the menu bar.

Other ways of calling view_zygo:
```matlab
% using the file name
view_zygo('/path/to/data/file.datx')
% or by loading data manually
z = load_zygo('/path/to/data/file.datx');
view_zygo(z)
```

### Note on set manipulations
There are some not terribly advanced methods for manipulating the data sets available in the GUI. These only operate on the _plotted_ sets and are not stored anywhere - if you want to save the manipulated data you have to export it (rightclick on it and select the type of export you want, file or to workspace). If you on the other hand want to revert your manipulations you simply select another set and then back again. If you wanna read more about the interpolation see `doc scatteredInterpolant`

### Note about javacomponent
Since R2019b matlab has started issuing warnings about javacomponent being deprecated, i've chosen to silence this warning in here. I dunno how mathwork's gonna deal with the whole oracle debacle but should they choose to deprecate javacomponent (and i would assume javaObjectEDT, etc.) shit's gonna break - even their warning says "There is no simple replacement for [javacomponent]"!

written by ragnar.seton@angstrom.uu.se

### TODO
See the bottom of the actual `help view_zygo` (or just look at the file if you're browsing the repo online).

## Screenshot
![screenshot](screenshot.png)

## Compatibility
Tested and works with MATLAB R2019a, R2019b, R2020a on macOS and linux, please let me know if you've successfully tested it in other versions of MATLAB or other OSs. Does not work with Octave unfortunately as it's missing some of the hdf5-functions.



## License
[BSD-2-Clause](https://opensource.org/license/bsd-2-clause/)